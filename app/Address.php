<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Address extends Model
{
    protected $guarded = ['id'];
    
    protected static function boot()
    {
        parent::boot();

        // static::creating(function () {
        //     Cache::forget('app.addresses.index');
        // });

        // static::updating(function ($address) {
        //     Cache::forget('app.addresses.index');
        // });

        // static::deleting(function () {
        //     Cache::forget('app.addresses.index');
        // });
    }

    public function addressable()
    {
      return $this->morphTo();
    }

    /**
     * Get the user's first name.
     *
     * @param  string  $value
     * @return string
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Set the data.
     *
     * @param  string  $value
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize(collect($value));
    }
}
