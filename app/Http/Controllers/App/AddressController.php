<?php
namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Address;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Cache::remember('app.addresses.index', 120, function () {
            return Address::latest()->get();
        });

        // $addresses = Address::latest()->get();

        return view('app.addresses.index', compact('addresses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  id
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {   
        return view('app.addresses.edit', compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  request
     * @param  int  id
     * @return \Illuminate\Http\Response
     */
    public function update(Address $address)
    {
        request()->validate([
            'postcode' => 'required'
        ]);
        
        $address->update([
            'data' => request()->except('_token', '_method'),
        ]);

        return redirect()->route('app.addresses.index')->withSuccess('Address Updated.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Address::create([
            'data' => request()->except('_token'),
            'model_id' => auth()->id(),
            'model_type' => 'App\User'
        ]);

        return redirect()->route('app.addresses.index')->withSuccess('Address Created.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $address->delete();
        return redirect()->back()->withSuccess('Address Removed.');
    }
}
