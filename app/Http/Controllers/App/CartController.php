<?php
namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cart = collect($request->session()->get('cart'));
        return view('app.cart.index', compact('cart'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $cart = collect($request->session()->get('cart'));

        $cart->first(function ($stored, $index) use ($product){
            if(collect($stored)->get('product') == $product) {
                $quantity = collect($stored)->get('quantity') + request()->quantity;
                request()->session()->put('cart.'.$index.'.quantity', $quantity);
                return true;
            }
        }, function() use ($product) {
            request()->session()->push('cart', [
                'quantity' => request()->quantity,
                'product' => $product,
            ]);
        });
        
        return redirect()->route('app.cart.index')->withSuccess('Product Added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  request
     * @param  int  id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        request()->session()->put('cart.'.$id.'.quantity', request()->quantity);
        return redirect()->back()->withSuccess('Cart Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        request()->session()->forget('cart.'.$id);
        return redirect()->back()->withSuccess('Product Removed.');
    }
}
