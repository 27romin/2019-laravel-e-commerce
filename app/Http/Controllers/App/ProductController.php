<?php
namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Product;

class ProductController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Cache::remember('app.product.index', 60, function () {
            return Product::latest()->get();
        });
        return view('app.products.index', compact('products'));
    }
}
