@extends('admin.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
    @include('admin.templates.partials.messages.success')
    <h2>Products</h2>
    <p>Flush cache if you have updated the products.</p>
	<p>
        <form action="{{ route('admin.cache.destroy', 'app.product.index') }}" method="POST">
             @csrf
             @method('DELETE')
             <button type="submit">Flush Cache</button>
         </form>
	</p>
@endsection
