@extends('admin.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h1>Admin Control Panel</h1>
	<p>
		 Welcome {{ auth()->user()->name }}
	</p>
@endsection
