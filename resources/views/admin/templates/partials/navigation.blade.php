<fieldset>
	<ul>
		<li><a href="{{ route('app.home.index') }}" target="_blank">App Home</a></li>
		<li><a href="{{ route('logout') }}"
			onclick="event.preventDefault();
						document.getElementById('logout-form').submit();">
			Logout
		</a></li>
	</ul>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>
</fieldset>

<fieldset>
	<legend>Navigation</legend>
	<ul>
		<li><a href="{{ route('admin.cache.index') }}">Cache</a></li>
	</ul>
</fieldset>