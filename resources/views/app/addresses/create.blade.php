@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <h2>Add Address</h2><hr>
    <form action="{{ route('app.addresses.store') }}" method="POST">
        @csrf
        <fieldset>
            <legend>Home Number</legend>
            <input type="text" name="home_number">
        </fieldset>
        <fieldset>
            <legend>Post code</legend>
            <input type="text" name="postcode">
        </fieldset>
        <button type="submit">create</button>
    </form>
@endsection
