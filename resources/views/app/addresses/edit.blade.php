@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <h2>Edit Address</h2><hr>
    @include('app.templates.partials.messages.errors')
    <form action="{{ route('app.addresses.update', $address) }}" method="POST">
        @csrf
        @method('PATCH')
        <fieldset>
            <legend>Home Number</legend>
            <input type="text" name="home_number" value="{{ old('home_number') ? old('home_number') : $address->data->get('home_number') }}">
        </fieldset>
        <fieldset>
            <legend>Post code</legend>
            <input type="text" name="postcode" value="{{ old('postcode') ? old('postcode') : $address->data->get('postcode') }}">
        </fieldset>
        <button type="submit">update</button>
    </form>
@endsection
