@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <a href="{{ route('app.addresses.create') }}">Add Address</a>
    <h2>Addresses</h2><hr>
    @include('app.templates.partials.messages.success')
    @foreach($addresses as $address)
        {{ $address->data->get('postcode') }}
        <a href="{{ route('app.addresses.edit', $address) }}">edit</a>
        <form action="{{ route('app.addresses.destroy', $address) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit">delete</button>
        </form>
        <hr>
    @endforeach
@endsection
