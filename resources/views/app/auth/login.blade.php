@extends('app.templates.default')

@section('content')
    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <fieldset>
            <legend>E-Mail Address</legend>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
        </fieldset>
                        
        <fieldset>
            <legend>Password</legend>
            <input id="password" type="password" name="password" required>
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
        </fieldset>

        <fieldset>
            <legend>Remember Me</legend>
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
        </fieldset>

        <button type="submit">
            Login
        </button>

        <p>
            <a href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
        </p>
    </form>
@endsection
