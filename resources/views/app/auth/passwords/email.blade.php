@extends('app.templates.default')

@section('content')
    @if (session('status'))
        {{ session('status') }}
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        
        <fieldset>
            <legend>E-Mail Address</legend>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
        </fieldset>

        <button type="submit">
            Send Password Reset Link
        </button>
    </form>
@endsection
