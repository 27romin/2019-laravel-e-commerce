@extends('app.templates.default')

@section('content')
    <form method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">

        <fieldset>
            <legend>E-Mail Address</legend>
            <input id="email" type="email" name="email" value="{{ $email or old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
        </fieldset>

        <fieldset>
            <legend>Password</legend>
            <input id="password" type="password" name="password" required>
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
        </fieldset>

        <fieldset>
            <legend>Confirm Password</legend>
            <input id="password-confirm" type="password" name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            @endif
        </fieldset>

        <button type="submit">
            Reset Password
        </button>
    </form> 
@endsection