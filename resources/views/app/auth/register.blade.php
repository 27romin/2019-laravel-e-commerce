@extends('app.templates.default')

@section('content')
    <form  method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <fieldset>
            <legend>Name</legend>
            <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <strong>{{ $errors->first('name') }}</strong>
            @endif
        </fieldset>

        <fieldset>
            <legend>E-Mail Address</legend>   
            <input id="email" type="email" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
        </fieldset>  

        <fieldset>
            <legend>Password</legend>
            <input id="password" type="password" name="password" required>
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
        </fieldset>

        <fieldset>
            <legend>Confirm Password</legend>
            <input id="password-confirm" type="password" name="password_confirmation" required>
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
        </fieldset>

        <button type="submit">
            Register
        </button>
    </form>
@endsection