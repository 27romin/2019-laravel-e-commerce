@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <h2>Cart</h2><hr>
    @include('app.templates.partials.messages.success')
    @foreach($cart as $id => $item)
        <form action="{{ route('app.cart.update', $id) }}" method="POST">
            @csrf
            @method('PATCH')
            <p>
                Name: {{ $item['product']->name }}
            </p>
            <p>
                Price: {{ $item['product']->price }}
            </p>
            <p>
                Quantity: <input type="text" name="quantity" value="{{ $item['quantity'] }}">
            </p>
            <p>
                <button type="submit">Update</button>
            </p>
        </form>

        <form action="{{ route('app.cart.destroy', $id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit">Remove</button>
        </form>
        <hr>
    @endforeach
@endsection
