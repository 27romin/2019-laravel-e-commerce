@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <h2>Products</h2><hr>
    @foreach($products as $product)
        <form action="{{ route('app.cart.store', $product) }}" method="POST">
            @csrf
            <p>
                Name: {{ $product->name }}
            </p>
            <p>
                Price: {{ $product->price }}
            </p>
            <p>
                Quantity: <input type="text" name="quantity" value="1">
            </p>
            <p>
                <button type="submit">Add</button>
            </p>
        </form>
        <hr>
    @endforeach
    {{-- {{ $products->links() }} --}}
@endsection
