<fieldset>
	<ul>
		@guest
			<li><a href="{{ route('login') }}">Login</a></li>
			<li><a href="{{ route('register') }}">Register</a></li>
		@else
			@if($administrator)
				<li><a href="{{ route('admin.home.index') }}">Admin Panel</a></li>
			@endif
			<li><a href="{{ route('logout') }}"
				onclick="event.preventDefault();
						document.getElementById('logout-form').submit();">
				Logout
			</a></li>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>
		@endguest
	</ul>
</fieldset>

<fieldset>
	<legend>Navigation</legend>
	<ul>
		<li><a href="{{ route('app.home.index') }}">Home</a></li>
		<li><a href="{{ route('app.cart.index') }}">Cart</a></li>
		<li><a href="{{ route('app.product.index') }}">Products</a></li>
		<li><a href="{{ route('app.addresses.index') }}">Address</a></li>
	</ul>
</fieldset>