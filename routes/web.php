<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return view('welcome');
});

Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('home', 'HomeController@index')->name('admin.home.index');
    Route::get('cache', 'CacheController@index')->name('admin.cache.index');
    Route::delete('cache/{key}', 'CacheController@destroy')->name('admin.cache.destroy');
});

Route::namespace('App')->prefix('home')->group(function () {
    Route::get('', 'HomeController@index')->name('app.home.index');
    Route::get('cart', 'CartController@index')->name('app.cart.index');
    Route::patch('cart/{id}', 'CartController@update')->name('app.cart.update');
    Route::delete('cart/{id}', 'CartController@destroy')->name('app.cart.destroy');
    Route::get('products', 'ProductController@index')->name('app.product.index');
    Route::post('cart/{product}', 'CartController@store')->name('app.cart.store');

    Route::resource('addresses', 'AddressController', ['as' => 'app'])->except(['show']);

	Auth::routes();
});